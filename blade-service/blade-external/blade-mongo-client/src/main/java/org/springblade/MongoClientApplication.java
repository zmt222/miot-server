package org.springblade;

import org.springblade.core.cloud.client.BladeCloudApplication;
import org.springblade.core.launch.BladeApplication;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

/**
 * MongoDB调用客户端
 * @author 李家民
 */
@BladeCloudApplication
public class MongoClientApplication {
	public static void main(String[] args) {
		BladeApplication.run("blade-mongo-client", MongoClientApplication.class, args);
	}
}
