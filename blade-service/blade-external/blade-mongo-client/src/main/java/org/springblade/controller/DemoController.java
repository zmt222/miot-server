package org.springblade.controller;

import org.springblade.common.tool.ThreadPoolUtils;
import org.springblade.core.tool.api.R;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * 测试用控制器
 * @author 李家民
 */
@RestController
public class DemoController {

	@Resource
	private MongoTemplate mongoTemplate;

	@PostMapping("/demo01")
	public R demo01(@RequestParam MultipartFile file) {

		// mongoTemplate.save(file);


		return R.status(true);
	}


}
