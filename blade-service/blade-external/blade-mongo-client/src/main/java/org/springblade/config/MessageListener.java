package org.springblade.config;

import com.alibaba.fastjson.JSONObject;
import org.springblade.common.config.MqQueueConfig;
import org.springblade.common.tool.DateTime;
import org.springblade.common.tool.SysResourceUtil;
import org.springblade.common.tool.ThreadPoolUtils;
import org.springblade.entity.SequenceDemo;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * MQ消息监听器
 * @author 李家民
 */
@Component
public class MessageListener {

	@Resource
	private MongoTemplate mongoTemplate;

	/**
	 * 时序数据插入
	 * @param message JSON DATA
	 */
	@RabbitListener(queues = MqQueueConfig.SEQUENCE_DATA_DEMO_QUEUE)
	public void logAuditQueue(Message message) {
		// 数据以Map形式过来 这边用实体接收即可
		SequenceDemo sequenceDemo = JSONObject.parseObject(new String(message.getBody()), SequenceDemo.class);
		mongoTemplate.save(sequenceDemo);
	}


	/**
	 * demo
	 */
	// @PostConstruct
	private void demo() {
		CompletableFuture.runAsync(() -> {
			try {
				Thread.sleep(20000);
				while (true) {
					String strDate = DateTime.simpleDateFormat.format(new Date());
					Map<String, Object> nowPerformance = SysResourceUtil.getNowPerformance();
					SequenceDemo sequenceDemo = new SequenceDemo();
					sequenceDemo.setValueMap(nowPerformance);
					sequenceDemo.setCreateTime(strDate);
					sequenceDemo.setSources("system");
					sequenceDemo.setType(1);
					mongoTemplate.save(sequenceDemo);
					Thread.sleep(60000);
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}, ThreadPoolUtils.getThreadPool());
	}


}
