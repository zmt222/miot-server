package org.springblade.config;

import org.springblade.common.config.MqQueueConfig;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.SimpleDateFormat;

/**
 * RabbitMQ配置
 * @author 李家民
 */
@Configuration
public class MqConfig {

	/**
	 * 交换机
	 * @return
	 */
	@Bean("bootExchange")
	public Exchange bootExchange() {
		return ExchangeBuilder.directExchange(MqQueueConfig.SEQUENCE_DATA_DEMO_EXCHANGE).durable(true).build();
	}

	/**
	 * 队列/路由键
	 * @return
	 */
	@Bean("bootQueue")
	public Queue bootQueue() {
		return QueueBuilder.durable(MqQueueConfig.SEQUENCE_DATA_DEMO_QUEUE).build();
	}

	/**
	 * 构造绑定
	 * @param queue
	 * @param exchange
	 * @return
	 */
	@Bean
	public Binding bindQueueExchange(@Qualifier("bootQueue") Queue queue,
									 @Qualifier("bootExchange") Exchange exchange) {
		return BindingBuilder.bind(queue).to(exchange).with(MqQueueConfig.SEQUENCE_DATA_DEMO_QUEUE).noargs();
		// rabbitTemplate.convertAndSend(RabbitMQConfig.EXCHANGE_NAME, "routingKeyForDemo01", "message = " + tempNum);
	}

}
