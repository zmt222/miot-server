package org.springblade.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Map;

/**
 * 时序数据实体-数据分析用
 * 实验阶段
 * @author 李家民
 */
@Document(collection = "SequenceDemo")

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SequenceDemo implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	@Id
	private String id;

	/** 数据来源 */
	private String sources;

	/** 创建时间 */
	private String createTime;

	/**
	 * 数据类型
	 * 1.系统性能监控
	 */
	private Integer type;

	/** 数据实体 */
	private Map<String, Object> valueMap;

}
