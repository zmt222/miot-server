package org.springblade.netty.websocket;


import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;
import org.springblade.netty.protocol.response.ResponseMsg;


/**
 * WebSocket 编码器
 * @author 李家民
 */
public class WebSocketBusinessMsgEncoder extends MessageToByteEncoder<ResponseMsg> {

	@Override
	protected void encode(ChannelHandlerContext channelHandlerContext, ResponseMsg responseMsg, ByteBuf byteBuf) throws Exception {
		// 协议特殊约定：必须走二进制流
		BinaryWebSocketFrame binaryWebSocketFrame = new BinaryWebSocketFrame(responseMsg.entireMsg(byteBuf));
		channelHandlerContext.writeAndFlush(binaryWebSocketFrame);
		// 引用计数处理 - 多出站处理器会导致资源过度释放
		byteBuf.retain();
		// 资源释放
		responseMsg.release();
	}
}


