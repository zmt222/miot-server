package org.springblade.mqtt;

/**
 * mqtt配置
 * @author 李家民
 */
public class MqttConfig {

	/** 链接地址 */
	public static final String IP_ADDRESS = "tcp://42.194.152.233:1883";
	/** 用户名 */
	public static final String USERNAME = "lijiamin";
	/** 密码 */
	public static final String PASSWD = "123456";

	/** QOS */
	public static final Integer QOS = 2;

	/** 订阅消息主题 - 可为多个 现指一个作为示例 */
	public static final String TOPIC = "test/topic";
}
