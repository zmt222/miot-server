package org.springblade;

import org.springblade.core.cloud.client.BladeCloudApplication;
import org.springblade.core.launch.BladeApplication;

/**
 * 设备运维服务
 * @author 李家民
 */
@BladeCloudApplication
public class EquipmentApplication {
	public static void main(String[] args) {
		BladeApplication.run("blade-equipment", EquipmentApplication.class, args);
	}
}
