package org.springblade.common.config;

/**
 * 公共MQ队列及交换机名称配置
 * @author 李家民
 */
public class MqQueueConfig {

	/** 能耗模块-交换机 */
	public static final String ENERGY_EXCHANGE = "ENERGY_EXCHANGE";
	/** 能耗模块-电表-数据-队列/路由键 */
	public static final String ENERGY_ELC_DATA_QUEUE = "ENERGY_ELC_DATA_QUEUE";

	/** 日志模块-交换机 */
	public static final String LOG_AUDIT_EXCHANGE = "LOG_AUDIT_EXCHANGE";
	/** 日志模块-队列/路由键 */
	public static final String LOG_AUDIT_QUEUE = "LOG_AUDIT_QUEUE";

	/** MongoDB-时序数据存储-交换机 */
	public static final String SEQUENCE_DATA_DEMO_EXCHANGE = "SEQUENCE_DATA_DEMO_EXCHANGE";
	/** MongoDB-时序数据存储-队列/路由键 */
	public static final String SEQUENCE_DATA_DEMO_QUEUE = "SEQUENCE_DATA_DEMO_QUEUE";

}
